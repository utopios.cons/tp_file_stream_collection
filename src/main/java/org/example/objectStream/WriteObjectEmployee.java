package org.example.objectStream;

import java.io.*;
import java.util.Date;

public class WriteObjectEmployee {

    private static String file_path = "/Users/mohamedaijjou/IdeaProjects/projet-java/src/main/java/org/example/objectStream/employee.data";

    public static void main() throws IOException {


        File outFile = new File(file_path);
        Employee e1 = new Employee("Tom", 1000f);
        Employee e2 = new Employee("Jerry", 2000f);
        Employee e3 = new Employee("Donald", 1200f);

        Employee[] employees = new Employee[]{e1, e2, e3};

        OutputStream os = new FileOutputStream(outFile);
        ObjectOutputStream oos = new ObjectOutputStream(os);

        System.out.println("Writing file: " + outFile.getAbsolutePath());

        oos.writeObject(new Date());
        oos.writeUTF("Employee data");

        oos.writeInt(employees.length);

        for (Employee e : employees) {
            oos.writeObject(e);
        }
        oos.close();
        System.out.println("Finished!");
    }


}
