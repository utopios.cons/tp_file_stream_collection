package org.example.objectStream;

import java.io.*;
import java.util.Date;

public class ReadObjectEmployee {

    private static String file_path = "/Users/mohamedaijjou/IdeaProjects/projet-java/src/main/java/org/example/objectStream/employee.data";

    public static void main() throws IOException, ClassNotFoundException {
        File inFile = new File(file_path);


        InputStream is = new FileInputStream(inFile);
        ObjectInputStream ois = new ObjectInputStream(is);

        System.out.println("Reading file: " + inFile.getAbsolutePath());
        System.out.println();

        Date date = (Date) ois.readObject();
        String info = ois.readUTF();

        System.out.println(date);
        System.out.println(info);
        System.out.println();

        int employeeCount = ois.readInt();

        System.out.println("La date d'ecriture : " + date);
        System.out.println("Nombre employee : " + employeeCount);

        for(int i=0; i< employeeCount; i++) {

            Employee e = (Employee) ois.readObject();
            System.out.println("Employee Name: " + e.getFullName() +" / Salary: " + e.getSalary());
        }
        ois.close();
    }

}
